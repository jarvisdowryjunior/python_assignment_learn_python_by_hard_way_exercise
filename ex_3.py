# Mathematical operator and symbols
# First variable 10
# Second variable 5

print(10+5)  # Addition of 10 and 5
print(10-5)  # Substraction of 10 and 5
print(10*5)  # Multiplication of 10 and 5
print(10/5)  # Division of 10 and 5
print(10%5)  # Mod of 10 and 5
print(10<5)  # Less than of 10 and 5
print(10>5)  # Greater than of 10 and 5
print(10<=5)  # Less than equal to of 10 and 5
print(10>=5)  # Greate than or equal to of 10 and 5